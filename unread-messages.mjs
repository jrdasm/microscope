import { computed, createTemplate } from "./microscope/index.mjs"

const template = createTemplate(`
<div class="toto">
  <h3>You have {{ newMessages.value.length }} new messages<!-- this is just a comment --></h3>
  <button @click="toggleCollapse">{{ listShown.value ? 'Collapse' : 'Expand' }}</button>
  <ul d-if="listShown.value" d-for="msg in newMessages.value">
    <li>
      <dt>{{ $index + 1 }} - {{ msg.subject }}</dt><dd>{{ msg.body }}</dd>
      <button @click="markAsRead($index)">mark as read</button>
    </li>
  </ul>
  <button d-if="listShown.value" @click="markAllAsNew">mark all as new</button>
</div>
`)

const UnreadMessages = {
  name: 'UnreadMessages',
  template,
  setup() {
    const messages = [
      { subject: "Title", body: "First message", read: false },
      { subject: "Other title", body: "Second message", read: false },
      { subject: "This is important", body: "Third message", read: false },
      { subject: "Notification from Mastodon", body: "Fourth message", read: true },
    ]
    let listShown = true
    return {
      newMessages: computed(() => messages.filter(msg => !msg.read)),
      listShown: computed(() => listShown),
      markAsRead (index) {
        console.log('markAsRead', index)
        const globalIndex = messages.map((msg, i) => ({...msg, i})).filter(msg => !msg.read)[index].i
        console.log('globalIndex', globalIndex)
        messages[globalIndex].read = true
        console.log('re-render')
        this.render()
      },
      markAllAsNew (event) {
        console.log('markAllAsNew', event)
        messages.forEach(msg => { msg.read = false })
        this.render()
      },
      toggleCollapse () {
        listShown = !listShown
        this.render()
      },
    }
  },
}
export { UnreadMessages }
