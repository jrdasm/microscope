import { createApp } from "./microscope/index.mjs"
import { App } from "./app.mjs"
createApp(App).mount('#app')
