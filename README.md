Microscope
==========

Micro framework, heavily inspired by [VueJS 3](https://vuejs.org/), to build tiny html pages with just **html** and **vanilla javascript**.

Modern browser required.

[⇒ Démo](https://jrdasm.gitlab.io/microscope/)
