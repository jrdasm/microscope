function toCamelCase(kebabCase) {
  if (kebabCase.includes('-')) {
    return kebabCase.toLowerCase().replace(/^./, c => c.toUpperCase()).replace(/-./, pos => pos[1].toUpperCase())
  } else {
    return kebabCase
  }
}

function addThisToKey(expression, key) {
  if (key.startsWith('$')) {
    key = key.replace('$', '\\$')
  }
  return expression.replaceAll(
    RegExp(
      `(^|[ \\t\\[(,])(${key})([^\\w]|$)`,
      'g',
    ),
    '$1this.$2$3',
  )
}

function safeEval(context, expression) {
  if (expression === undefined) {
    return ''
  }
  let exprWithThis = expression
  for (const key of Object.keys(context)) {
    exprWithThis = addThisToKey(exprWithThis, key)
  }
  // this is not safe eval, but it could be replaced by a parser and evaluator
  try {
    return (function(expr) {
      return eval(expr)
    }.bind(context))(exprWithThis)
  } catch {
    return `[ ${expression} ]`
  }
}

export {
  toCamelCase,
  safeEval,
}
