import { Component } from './component.mjs'
import { App } from './app.mjs'

function defineComponent(comp) {
  return new Component(comp)
}
function createApp(comp) {
  return new App(comp)
}
function createTemplate(templateStr) {
  return new DOMParser().parseFromString(templateStr.trim(), 'text/html').body
}
function reactive(obj) {
  // TODO proxy
  return obj
}
function ref(value) {
  return reactive({ value })
}
function computed(lambda) {
  return {
    get value() {
      return lambda()
    },
  }
}

export {
  defineComponent,
  createApp,
  createTemplate,
  reactive,
  ref,
  computed,
}
