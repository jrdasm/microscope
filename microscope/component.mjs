import { DomToAst } from './dom-to-ast.mjs'
import { VDomRenderer } from './vdom-renderer.mjs'
import { DomDiffApplier } from './dom-diff-applier.mjs'

class Component {
  constructor(options) {
    if (!options.name) {
      throw "name should be defined"
    }
    this.template = options.template ?? null
    this.components = options.components ?? null
    if (options.setup) {
      this.data = options.setup()
    } else {
      this.data = {}
    }
    this._parentEl = null
    this._indexEl = null
    this._renderVDom = null
    this._previousVDom = null
  }

  get domEl() {
    if (this._parentEl && this._indexEl) {
      return this._parentEl.childNodes[this._indexEl]
    } else {
      return null
    }
  }

  _parseTemplate() {
    if (this.template) {
      let domEl = null
      if (this.template.toString() === this.template) {
        const tpl = document.querySelector(this.template)
        domEl = tpl?.content ?? tpl  // use shadow dom or direct component
      } else {
        domEl = this.template
      }
      return new DomToAst(domEl, this.components).toAST()
    } else {
      return null
    }
  }

  _createRenderVDom() {
    if (this._renderVDom) {
      return
    }
    const ast = this._parseTemplate()
    if (ast) {
      this._renderVDom = new VDomRenderer(ast, this.components).createRenderVDomFunc()
      this._previousVDom = null
    }
  }

  attachToDom(domEl) {
    if (domEl && !this._parentEl) {
      this._parentEl = domEl.parentNode
      this._indexEl = [...this._parentEl.childNodes].findIndex(child => child === domEl)
    }
  }

  render () {
    if (this._parentEl) { // do nothing if not attached to DOM
      const vDom = this._renderVDom(this.data, this)
      new DomDiffApplier(this._parentEl, this._indexEl).apply(this._previousVDom, vDom)
      this._previousVDom = vDom
    }
  }

  mount(domOrQuery) {
    this._createRenderVDom()
    if (!this.parentEl) { // if not already mounted
      const domEl = domOrQuery.nodeType ? domOrQuery : document.querySelector(domOrQuery)
      this.attachToDom(domEl)
    }
  }
}

export { Component }
