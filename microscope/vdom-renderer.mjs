import { safeEval } from './utils.mjs'
import { Component } from './component.mjs'

class VDomRenderer {
  constructor(ast, components) {
    this.ast = ast
    this.components = components
  }

  interpolateText(state, content, children) {
    if (content == null && children.length) {
      return children.map(child => {
        if (child.type == 'EXPR') {
          return safeEval(state, child.expression)
        } else {
          return child.content
        }
      }).join('')
    } else {
      return content || ''
    }
  }

  createVText(text) {
    return {
      type: 'TEXT',
      text: text,
    }
  }

  createVComment(comment) {
    return {
      type: 'COMMENT',
      text: comment,
    }
  }

  createVAttribute(state, name, value) {
    return {
      type: 'ATTR',
      name,
      value,
    }
  }

  createVEvent(state, component, name, lambda, value) {
    const { handler, listenOn, stopListeningFrom } = lambda(state, component) || {}
    if (handler) {
      return {
        type: 'EVENT',
        name,
        handler,
        listenOn,
        stopListeningFrom,
        value,
      }
    } else {
      return null
    }
  }

  createVElement(name, props, events, children, component) {
    return {
      type: 'ELEMENT',
      name,
      props,
      events,
      children,
      component,
    }
  }

  applyDirectives(name, props, ast_children, directives, events, components, state, component) {
    const childAndStates = ast_children.map(child => ({
      child,
      state,
    }))
    for (const { name: dirName, lambda } of directives) {
      switch (dirName) {
        case 'if':
          if (!lambda(state)) {
            name = null
          }
          break
        case 'for':
          childAndStates.length = 0
          const nbInitalChildren = ast_children.length
          for (const subState of lambda(state)) {
            childAndStates.push(
              ...ast_children.map(child => ({
                child,
                state: subState,
              }))
            )
          }
          break
      }
    }
    if (name) {
      const children = childAndStates.map(
        ({ child, state }) => this.createVDom(child, components, state, component),
      ).filter(child => child)
      return this.createVElement(name, props, events, children, component)
    } else {
      return null
    }
  }

  createVDom(ast, components, state, component) {
    if (!ast) {
      return null
    }
    if (Array.isArray(ast)) {
      return ast.map(child => this.createVDom(child, components, state, component)).filter(child => child)
    }
    switch (ast.type) {
      case 'TEXT':
        const text = this.interpolateText(state, ast.content, ast.children)
        return this.createVText(text)
        break
      case 'COMMENT':
        const comment = ast.content
        return this.createVComment(comment)
        break
      case 'ELEMENT':
        const name = ast.tag
        const props = ast.props.map(prop => this.createVAttribute(state, prop.name, prop.value)).filter(prop => prop)
        const events = ast.events.map(ev => this.createVEvent(state, component, ev.name, ev.lambda, ev.value)).filter(ev => ev)
        return this.applyDirectives(name, props, ast.children, ast.directives, events, components, state, component)
        break
      case 'COMPONENT':
        const compOptions = components[ast.tag]
        if (compOptions) {
          const comp = new Component(compOptions)
          comp._createRenderVDom()
          const vdom = comp._renderVDom ? comp._renderVDom(comp.data, comp) : null
          comp._previousVDom = vdom
          return vdom
        } else {
          return null
        }
      default:
        return null
    }
  }

  createRenderVDomFunc() {
    return (state, component) => this.createVDom(this.ast, this.components, state, component)
  }
}

export { VDomRenderer }
