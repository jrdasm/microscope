import { toCamelCase, safeEval } from './utils.mjs'

class DomToAst {
  constructor(element, components) {
    this.root = element
    this.components = components ?? []
    this.componentNames = Object.keys(this.components)
  }

  parseText(el) {
    let content = el.textContent
    let children = []
    if (content.includes('{{') || content.includes('}}')) {
      children = content.split(/(\{\{.+?\}\})/).map(txt => {
        if (txt.startsWith('{{') && txt.endsWith('}}')) {
          return {
            type: 'EXPR',
            children: [],
            expression: txt.substring(2, txt.length - 2).trim(),
          }
        } else {
          return {
            type: 'TEXT',
            children: [],
            content: txt,
          }
        }
      })
      content = null
    }
    return {
      type: 'TEXT',
      children,
      content,
    }
  }

  parseComment(el) {
    return {
      type: 'COMMENT',
      content: el.textContent,
    }
  }

  isDirective(name) {
    return name.startsWith('d-') ? name.substring(2) : false
  }

  isEvent(name) {
    return name.startsWith('@') ? name.substring(1) : false
  }

  parseAttribute(attr) {
    const name = attr.nodeName
    if (!this.isDirective(name) && !this.isEvent(name)) {
      return {
        type: 'ATTR',
        name,
        value: attr.nodeValue,
      }
    } else {
      return null
    }
  }

  parseIf(value) {
    return state => !!safeEval(state, value)
  }

  parseFor(value) {
    if (!value.includes(' in ')) {
      throw `'element in list' format not found in for directive: ${value}`
    }
    const [ varName, iterable ] = value.split(' in ')
    return state => {
      const maybeList = safeEval(state, iterable)
      const lst = maybeList && Array.isArray(maybeList) ? maybeList : []
      return lst.map((item, $index) => ({
        ...state,
        [varName]: item,
        $index,
        $len: lst.length,
      }))
    }
  }

  parseDirective(attr) {
    const name = this.isDirective(attr.nodeName)
    if (name) {
      let lambda = null
      switch (name) {
        case 'if':
          lambda = this.parseIf(attr.nodeValue)
          break
        case 'for':
          lambda = this.parseFor(attr.nodeValue)
          break
      }
      if (lambda) {
        return {
          type: 'DIRECTIVE',
          name,
          lambda,
        }
      } else {
        return null
      }
    } else {
      return null
    }
  }
  
  parseNativeEvent(name, value) {
    return (state, component) => {
      let listener = null
      let args = null
      if (value && value.includes('(') && value.includes(')')) {
        const [expression, remain] = value.split('(')
        listener = safeEval(state, expression)
        args = remain.split(')')[0].split(',').map(arg => safeEval(state, arg))
      } else if (value && value.trim()) {
        listener = safeEval(state, value.trim())
        args = []
      }
      if (listener) {
        const boundListener = listener.bind(component)
        let handler = event => boundListener(...args, event)
        return {
          handler,
          listenOn: domEl => domEl.addEventListener(name, handler),
          stopListeningFrom: domEl => domEl.removeEventListener(name, handler),
        }
      } else {
        return null
      }
    }
  }

  parseEvent(attr) {
    const name = this.isEvent(attr.nodeName)
    const value = attr.nodeValue
    if (name) {
      let lambda = null
      switch (name) {
        case 'click':
          lambda = this.parseNativeEvent(name, value)
          break
      }
      if (lambda) {
        return {
          type: 'EVENT',
          name,
          lambda,
          value,
        }
      } else {
        return null
      }
    } else {
      return null
    }
  }

  parseElement(el) {
    const tag = toCamelCase(el.nodeName)
    const type = this.componentNames.includes(tag) ? 'COMPONENT' : 'ELEMENT'
    const attrList = [...(el.attributes ?? [])]
    const props = attrList.map(attr => this.parseAttribute(attr)).filter(prop => prop)
    const directives = attrList.map(attr => this.parseDirective(attr)).filter(directive => directive)
    const events = attrList.map(attr => this.parseEvent(attr)).filter(ev => ev)
    const children = [...el.childNodes].map(el => this.parse(el)).filter(el => el != null)
    return {
      type,
      tag,
      props,
      directives,
      events,
      children,
    }
  }

  parse(el) {
    const nodeType = el.nodeType
    switch (nodeType) {
      case Element.TEXT_NODE:
        return this.parseText(el)
      case Element.COMMENT_NODE:
        return this.parseComment(el)
      case Element.ELEMENT_NODE:
        return this.parseElement(el)
      case Element.DOCUMENT_FRAGMENT_NODE:
        const childList = [...el.children].map(childEl => this.parse(childEl))
        if (childList.length == 0) {
          return null
        } else if (childList.length == 1) {
          return childList[0]
        } else {
          return childList
        }
      default:
        return null
    }
  }

  toAST() {
    return this.parse(this.root)
  }
}

export { DomToAst }
