class DomDiffApplier {
  constructor(parentEl, index) {
    this.parentEl = parentEl
    this.index = index
  }

  _createChildElement(vdom) {
    switch (vdom.type) {
      case 'TEXT':
        return document.createTextNode(vdom.text)
      case 'COMMENT':
        return document.createComment(vdom.comment)
      case 'ELEMENT':
        const child = document.createElement(vdom.name)
        for (const prop of vdom.props) {
          child[prop.name] = prop.value
        }
        return child
      default:
        return null
    }
  }

  _createDummyChild() {
    return document.createComment('removed element')
  }

  _appendChild(parentEl, childEl) {
    if (parentEl && childEl) {
      parentEl.appendChild(childEl)
    }
  }

  _replaceChildBy(parentEl, oldChildEl, newChildEl) {
    if (parentEl && oldChildEl && newChildEl) {
      parentEl.replaceChild(newChildEl, oldChildEl)
    }
  }

  _replaceOrAppendChild(parentEl, oldChildEl, newChildEl) {
    if (parentEl && newChildEl) {
      if (oldChildEl) {
        this._replaceChildBy(parentEl, oldChildEl, newChildEl)
      } else {
        this._appendChild(parentEl, newChildEl)
      }
    }
  }

  _applyVDomDiff(parentEl, index, prevVdom, newVDom) {
    const currentNode = parentEl.childNodes[index]
    if (!newVDom) { // replace old node with a comment to keep index
      this._replaceOrAppendChild(parentEl, currentNode, this._createDummyChild())
    } else if (!prevVdom) { // first render
      this._replaceOrAppendChild(parentEl, currentNode, this._createChildElement(newVDom))
    } else if (Array.isArray(prevVdom) != Array.isArray(newVDom)) {
      // TODO
    } else if (prevVdom.type !== newVDom.type) {
      this._replaceOrAppendChild(parentEl, currentNode, this._createChildElement(newVDom))
    } else {
      switch (prevVdom.type) {
        case 'TEXT':
          if (currentNode) {
            if (prevVdom.text != newVDom.text) {
              currentNode.textContent = newVDom.text
            }
          } else {
            this._replaceOrAppendChild(parentEl, null, this._createChildElement(newVDom))
          }
          break
        case 'COMMENT':
          if (currentNode) {
            if (prevVdom.comment != newVDom.comment) {
              currentNode.textContent = newVDom.comment
            }
          } else {
            this._replaceOrAppendChild(parentEl, null, this._createChildElement(newVDom))
          }
          break
        case 'ELEMENT':
          const newPropNames = newVDom.props.map(prop => prop.name)
          for (const prop of prevVdom.props.filter(prop => !newPropNames.includes(prop.name))) {
            delete currentNode[prop.name]
          }
          for (const prop of newVDom.props) {
            currentNode[prop.name] = prop.value
          }
          break
      }
    }
    if (newVDom?.type == 'ELEMENT') {
      const newNode = parentEl.childNodes[index]
      // attach element to DOM
      newVDom.component.attachToDom(newNode)
      // events
      for (const ev of (prevVdom?.events ?? [])) {
        ev.stopListeningFrom(newNode)
      }
      for (const ev of newVDom.events) {
        ev.listenOn(newNode)
      }
      // children
      const maxChildren = Math.max(
        newVDom.children.length,
        prevVdom?.children?.length ?? 0,
      )
      for (let subIndex = 0; subIndex < maxChildren; subIndex++) {
        this._applyVDomDiff(
          newNode,
          subIndex,
          prevVdom?.children?.[subIndex] ?? null,
          newVDom.children[subIndex],
        )
      }
    }
  }

  apply(prevVdom, newVDom) {
    console.log(newVDom)
    this._applyVDomDiff(this.parentEl, this.index, prevVdom, newVDom)
  }
}

export { DomDiffApplier }
