import { Component } from './component.mjs'

class App extends Component {
  mount(querySelector) {
    if (!this.template) {
      this.template = querySelector
      this._createRenderVDom()
    }
    super.mount(querySelector)
    this.render()
  }
}

export { App }
