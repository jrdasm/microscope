import { createTemplate } from "./microscope/index.mjs"
import { UnreadMessages } from "./unread-messages.mjs"
const template = createTemplate(`
<h1>Hello {{ user.name }}</h1>
<span>Your role: <b>{{ user.role.toUpperCase() }}</b></span>
<unread-messages />
`)
const App = {
  name: 'App',
  template,
  components: {
    UnreadMessages,
  },
  setup() {
    return { user: { name: 'Cyrille', role: 'Admin' } };
  },
}
export { App }
